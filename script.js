function d(...args) {
    console.log(...args.map(x => JSON.stringify(x)));
    return args[0];
}

function dd(value, ...rest) {
    console.log(value, ...rest);
    return value;
}

function dh(x, suffix = "h") {
    const result = x.toString(16).toUpperCase();
    return `${result.padStart(Math.ceil(result.length/2)*2, "0")}${suffix}`;
}

const [TurboRandom, TurboRandomReset, TurboRandomGetSeed] = ((p, q) => [() => {
    const [p_, q_] = [p * 0x4E35n, q * 0x4E35n];
    const x = 0xFFFF_FFFFn & q_;
    p = 0xFFFF_FFFFn & (
        (q_ >> 32n) + q * 0x15A4n + p_
        + BigInt(x > 0xFFFF_FFFEn));
    q = 0xFFFF_FFFFn & (x + 1n);
    return 0x7FFF_FFFFn & p;
}, (seed) => {
    p = seed >> 32n;
    q = seed & 0xFFFF_FFFFn;
}, () => {
    return p << 32n | q;
}])(1n);

function succ(delta) {
    // 0, 1, -1, 2, -2, 3, -3, ...
    return delta == 0n ? 1n : delta < 0n ? -delta + 1n : -delta;
}

function cmp(p, q) {
    return p > q || -(p < q);
}

function abs(x) {
    return x < 0n ? -x : x;
}

function sgn(x) {
    return x < 0n ? -1 : x > 0n ? 1 : 0;
}

function FillZeroToCountAndShuffle(n) {
    const result = [...Array(n).keys()];
    for (let i = 0; i < 10 * n; i++) {
        const p = TurboRandom() % BigInt(n);
        const q = TurboRandom() % BigInt(n);
        const swap = result[p];
        result[p] = result[q];
        result[q] = swap;
    }
    return result;
}

function WorldGenPartIds(apc, vpc) {
    return FillZeroToCountAndShuffle(apc).map(x => x < vpc ? x : 1000);
}

function WorldGenLocations(n, r, c, pred = l => l % 2 == 0) {
    return FillZeroToCountAndShuffle(r * c).filter(pred).slice(0, n);
}

function zip(main, ...rest) {
    return main.map((x, i) => [x, ...rest.map(y => y[i])]);
}

async function load(path) {
    const octets = await (await (await fetch(path)).text())
        .split(" ").map(x => parseInt(x, 16));
    const result = new DataView(new ArrayBuffer(octets.length));
    for (const [i, x] of octets.entries())
        result.setUint8(i, x);
    return result;
}

async function loadraw(path) {
    return new DataView(await (await fetch(path)).arrayBuffer());
}

function vbyte(view, offset) {
    return view.getUint8(offset);
}

function vword(view, offset) {
    return view.getUint16(offset, true);
}

function vdword(view, offset) {
    return view.getUint32(offset, true);
}

function vslice(view, offset, len) {
    return new DataView(view.buffer, view.byteOffset + offset, len);
}

function vsplit(view, size) {
    const result = [];
    for (let i = 0; i < view.byteLength / size; i++)
        result.push(vslice(view, i * size, size));
    return result;
}

function vdump(view) {
    return [...Array(view.byteLength).keys()].map(o => view.getUint8(o));
}

function vdumpw(view) {
    return [...Array(view.byteLength/2).keys()].map(o => vword(view, o*2));
}

function vxd(view) {
    const displayTable =
        /*            _0    _1    _2    _3    _4    _5    _6    _7    _8    _9    _a    _b    _c    _d    _e    _f  */
        /* 0_ */ '\u2400\u263A\u263B\u2665\u2666\u2663\u2660\u2022\u25D8\u25CB\u25D9\u2642\u2640\u266A\u266B\u263C' +
        /* 1_ */ '\u25BA\u25C4\u2195\u203C\u00B6\u00A7\u25AC\u21A8\u2191\u2193\u2192\u2190\u221F\u2194\u25B2\u25BC' +
        /* 2_ */ '\u0020\u0021\u0022\u0023\u0024\u0025\u0026\u0027\u0028\u0029\u002A\u002B\u002C\u002D\u002E\u002F' +
        /* 3_ */ '\u0030\u0031\u0032\u0033\u0034\u0035\u0036\u0037\u0038\u0039\u003A\u003B\u003C\u003D\u003E\u003F' +
        /* 4_ */ '\u0040\u0041\u0042\u0043\u0044\u0045\u0046\u0047\u0048\u0049\u004A\u004B\u004C\u004D\u004E\u004F' +
        /* 5_ */ '\u0050\u0051\u0052\u0053\u0054\u0055\u0056\u0057\u0058\u0059\u005A\u005B\u005C\u005D\u005E\u005F' +
        /* 6_ */ '\u0060\u0061\u0062\u0063\u0064\u0065\u0066\u0067\u0068\u0069\u006A\u006B\u006C\u006D\u006E\u006F' +
        /* 7_ */ '\u0070\u0071\u0072\u0073\u0074\u0075\u0076\u0077\u0078\u0079\u007A\u007B\u007C\u007D\u007E\u2302' +
        /* 8_ */ '\u00C7\u00FC\u00E9\u00E2\u00E4\u00E0\u00E5\u00E7\u00EA\u00EB\u00E8\u00EF\u00EE\u00EC\u00C4\u00C5' +
        /* 9_ */ '\u00C9\u00E6\u00C6\u00F4\u00F6\u00F2\u00FB\u00F9\u00FF\u00D6\u00DC\u00A2\u00A3\u00A5\u20A7\u0192' +
        /* a_ */ '\u00E1\u00ED\u00F3\u00FA\u00F1\u00D1\u00AA\u00BA\u00BF\u2310\u00AC\u00BD\u00BC\u00A1\u00AB\u00BB' +
        /* b_ */ '\u2591\u2592\u2593\u2502\u2524\u2561\u2562\u2556\u2555\u2563\u2551\u2557\u255D\u255C\u255B\u2510' +
        /* c_ */ '\u2514\u2534\u252C\u251C\u2500\u253C\u255E\u255F\u255A\u2554\u2569\u2566\u2560\u2550\u256C\u2567' +
        /* d_ */ '\u2568\u2564\u2565\u2559\u2558\u2552\u2553\u256B\u256A\u2518\u250C\u2588\u2584\u258C\u2590\u2580' +
        /* e_ */ '\u03B1\u00DF\u0393\u03C0\u03A3\u03C3\u00B5\u03C4\u03A6\u0398\u03A9\u03B4\u221E\u03C6\u03B5\u2229' +
        /* f_ */ '\u2261\u00B1\u2265\u2264\u2320\u2321\u00F7\u2248\u00B0\u2219\u00B7\u221A\u207F\u00B2\u25A0\uFB00';
    return vdump(view).reduce((r,x) => r + displayTable[x], "");
}

function mg(map, key, initial = null) {
    return (map.has(key) ? map : map.set(key, initial)).get(key);
}

function mt(map, key, fun, initial = null) {
    return (map.has(key) ? map : map.set(key, initial)).set(key, fun(map.get(key)));
}

function ctype(canvas, e, areaIndex) {
    const type = Entity.types[e.type];
    const text = e.isTeleport
        ? e.teleportDestination?.toString() ?? "?"
        : type?.label ?? `${e.type.toString(16).toUpperCase()}h`;

    const padding = 3;
    const x = e.isTeleport && e.isLeftTeleport ? e.x + padding
        : e.isTeleport && e.isRightTeleport ? e.x + e.w - padding
        : e.x + e.w / 2;
    let y = e.isTeleport ? e.y + e.h / 2 : e.y + padding;
    canvas.font = `bold ${type?.size ?? 12}px sans-serif`;
    canvas.fillStyle = text == "?" ? "#00000040" : "#000000FF";
    canvas.textBaseline = e.isTeleport ? "middle" : "top";
    canvas.textAlign = e.isTeleport && e.isLeftTeleport ? "left"
        : e.isTeleport && e.isRightTeleport ? "right"
        : "center";

    if (type?.nowrap ?? false) {
        canvas.fillText(text, x, y);
        return;
    }

    for (let i = 0, j = text.length; i < text.length; i = j, j = text.length) {
        while (j > i + 1 && canvas.measureText(text.slice(i, j)).width > e.w)
            j -= 1;
        const fragment = text.slice(i, j);
        canvas.fillText(fragment, x, y);
        const metrics = canvas.measureText(fragment);
        y += Math.abs(metrics.actualBoundingBoxDescent) + Math.abs(metrics.actualBoundingBoxAscent) + padding;
    }
}

function CheckRectIntersects(p, q) {
    let result = 0;
    const px = p.x, pxMax = px + p.w, py = p.y, pyMax = py + p.h;
    const qx = q.x, qxMax = qx + q.w, qy = q.y, qyMax = qy + q.h;
    if (px < qxMax && qx < pxMax && py < qyMax && qy < pyMax) {
        result = Number(px < qx) << 0;
        result |= Number(qxMax < pxMax) << 1;
        result |= Number(py < qy) << 2;
        result |= Number(qyMax < pyMax) << 3;
        result = result == 0 ? 0b1111 : result;
    }
    return result;
}

class Level {
    levelId;

    // AREA 579h + [0..9)
    world = null;

    // PART 3E8h + [0..15)
    parts = new PartResource(this);

    // INTS 582h + [0..15)
    areaPartsCounts = [];

    constructor(levelId) {
        this.levelId = levelId;
    }

    load(GIZMO, worlds) {
        this.world = worlds[[0, 1, 1, 2, 2, 3, 4, 4, 5, 5, 6, 7, 7, 8, 8][this.levelId]];
        this.parts.read(GIZMO.get("PART", 0x3E8 + this.levelId), 0);

        const view = GIZMO.get("INTS", 0x582 + this.levelId);
        for (let offset = 0; offset < view.byteLength; void offset) {
            this.areaPartsCounts.push(vword(view, offset));
            offset += 2;
        }
    }

    get worldPartsCount() {
        return this.areaPartsCounts[0];
    }

    get spawnAreaPartsCount() {
        return this.areaPartsCounts[this.world.spawnAreaId];
    }

    generatePartIds() {
        return WorldGenPartIds(this.worldPartsCount, this.parts.definitionCount);
    }

    place(partIds) {
        const result = [];
        for (const [i, area] of this.world.areas.entries()) {
            const areaPartsCount = this.areaPartsCounts[i];
            if (i == 0 || areaPartsCount == 0) continue;
            const locations = WorldGenLocations(
                areaPartsCount, area.rows, area.columns,
                l => area.canPlacePart(l));
            for (const location of locations)
                result.push([partIds.shift(), area.areaIndex, location]);
        }
        return result;
    }

    generateAndPlace(seed) {
        TurboRandomReset(seed);
        return this.place(this.generatePartIds());
    }
}

class World {
    areaCount;
    areas = [];

    read(view, offset) {
        this.areaCount = vword(view, offset);
        offset += 2;

        for (let i = 0; i < this.areaCount; i++) {
            const AreaKind = i > 3 ? ReverseArea : ObverseArea;
            const area = new AreaKind(i, this);
            offset = area.read(view, offset);
            this.areas.push(area);
        }

        return offset;
    }

    get pageCount() {
        return (this.areaCount / 4) - 1;
    }

    get spawnAreaId() {
        return this.pageCount;
    }

    get spawnArea() {
        return this.areas[this.spawnAreaId];
    }
}

class Area {
    world;
    areaIndex;
    structuralGroups = [];
    ladderGroups = [];
    cEntityGroups = [];
    dEntityGroups = [];
    eEntityGroups = [];

    rows; columns;
    rowStart; columnStart;
    rowHeight; columnWidth;
    xLeftForPart; xPadForPart; yTopForPart;
    partWidth; partHeight;
    yTopForFloor;

    constructor(areaIndex, world = null) {
        this.areaIndex = areaIndex;
        this.world = world;
    }

    read(view, offset) {
        const baseOffset = offset;
        offset += 288;

        const entityCount = (j, countsOffset) => {
            return vword(view, baseOffset + countsOffset + j * 2);
        }

        const readEntity = (group, EntityKind) => {
            const entity = new EntityKind(this);
            offset = entity.read(view, offset);
            group.push(entity);
        }

        const readEntities = (j, field, countsOffset, ...rest) => {
            const count = entityCount(j, countsOffset);
            field[j] = [];
            for (let i = 0; i < count; i++)
                readEntity(field[j], ...rest);
        }

        for (const j of [3, 2, 1, 0])
            readEntities(j, this.structuralGroups, 2, StructuralEntity);

        for (const j of [0, 1, 2, 3])
            readEntities(j, this.ladderGroups, 10, LadderEntity);

        for (const j of [0, 1, 2, 3, 4, 5])
            readEntities(j, this.cEntityGroups, 18, EntityC);

        for (const j of [0, 1, 2, 3, 4, 5])
            readEntities(j, this.dEntityGroups, 54, EntityD);

        for (const j of [0, 1, 2, 3, 4, 5])
            readEntities(j, this.eEntityGroups, 90, EntityE);

        return offset;
    }

    partSpriteRect(location) {
        const x = (location % this.columns | 0) * this.columnWidth + this.xLeftForPart + this.xPadForPart;
        const y = (location / this.columns | 0) * this.rowHeight + this.yTopForPart;
        const w = this.partWidth;
        const h = this.partHeight;
        return {x,y,w,h};
    }

    partCollisionRect(location) {
        const x = (location % this.columns | 0) * this.columnWidth + this.xLeftForPart;
        const y = (location / this.columns | 0) * this.rowHeight + this.yTopForPart;
        const w = this.columnWidth;
        const h = this.partHeight;
        return {x,y,w,h};
    }

    canPlaceEntity(entity) {
        return this.structuralGroups[0].every(e => {
            return e.type <= 3 || CheckRectIntersects(entity, e) == 0;
        }) && this.ladderGroups[0].every(e => {
            return CheckRectIntersects(entity, e) == 0;
        });
    }

    canPlacePart(location) {
        return this.canPlaceEntity(this.partCollisionRect(location));
    }
}

class ObverseArea extends Area {
    rows = 6; columns = 15;
    rowStart = 14; columnStart = 16;
    rowHeight = 50; columnWidth = 32;
    xLeftForPart = 16; xPadForPart = 16; yTopForPart = 52;
    partWidth = 16; partHeight = 13;
    yTopForFloor = 49+17-4; floorHeight = 4;
}

class ReverseArea extends Area {
    rows = 3; columns = 7;
    rowStart = 14; columnStart = 24;
    rowHeight = 100; columnWidth = 64;
    xLeftForPart = 24; xPadForPart = 32; yTopForPart = 88;
    partWidth = 32; partHeight = 24;
    yTopForFloor = 87+29-8; floorHeight = 8;
}

class Entity {
    static types = [
        /* 00h */ [],
        /* 01h */ [],
        /* 02h */ [],
        /* 03h */ ["background"],
        /* 04h */ ["gap (obverse)", "↓", 24],
        /* 05h */ ["gap (reverse stay)", "↓", 24],
        /* 06h */ ["gap (reverse tp-down)", "↓↓", 24],
        /* 07h */ ["puzzle (locked)", "#", 18, true],
        /* 08h */ ["pipe (tail)"],
        /* 09h */ ["puzzle (unlocked)", "@", 18, true],
        /* 0Ah */ ["gate (available tp-left)", "", 18, true],
        /* 0Bh */ ["gate (available tp-right)", "", 18, true],
        /* 0Ch */ ["gate (blocked tp-left)", "", 18, true],
        /* 0Dh */ ["gate (blocked tp-right)", "", 18, true],
        /* 0Eh */ [],
        /* 0Fh */ [],
        /* 10h */ ["wall", "WALL"],
        /* 11h */ [],
        /* 12h */ ["pipe (body)"],
        /* 13h */ ["ladder (vent)", "↑", 24],
        /* 14h */ ["ladder (trampoline)", "↑↑", 24],
        /* 15h */ ["ladder (paddle up-right)", "↗", 24],
        /* 16h */ ["ladder (paddle up-left)", "↖", 24],
    ].map(([description, label, size, nowrap]) => ({description, label, size, nowrap}));

    area;
    x; y; w; h;
    type = null;

    len;

    constructor(area = null) {
        this.area = area;
    }

    read(view, offset) {
        this.x = vword(view, offset + 0);
        this.y = vword(view, offset + 2);
        this.w = vword(view, offset + 4);
        this.h = vword(view, offset + 6);
        return offset + this.len;
    }

    get isTeleport() {
        return false;
    }
}

class StructuralEntity extends Entity {
    len = 32;
    spriteId;

    read(view, offset) {
        this.spriteId = vword(view, offset + 14);
        this.type = vword(view, offset + 16);
        return super.read(view, offset);
    }

    get isTeleport() {
        return this.isDoorTeleport || this.isLeftTeleport || this.isRightTeleport;
    }

    get isBlockedTeleport() {
        return [0x0C,0x0D].includes(this.type);
    }

    get isDoorTeleport() {
        return [0x07,0x09].includes(this.type);
    }

    get isLeftTeleport() {
        return [0x0A,0x0C].includes(this.type);
    }

    get isRightTeleport() {
        return [0x0B,0x0D].includes(this.type);
    }

    get teleportDestination() {
        //  3   2   1
        //  5   4   9   8   13  12
        //  7   6   11  10  15  14
        const areaIndex = this.area.areaIndex;
        if (this.isDoorTeleport)
            return areaIndex > 3
                ? {12:3,13:3,8:2,9:2,4:1,5:1,14:3,15:3,10:2,11:2,6:1,7:1}[areaIndex]
                : areaIndex * 4 + Number(this.y >= 164) * 2 + Number(this.x >= 512/2);
        else if (this.isLeftTeleport)
            return {2:3,1:2,12:13,13:8,8:9,9:4,4:5,14:15,15:10,10:11,11:6,6:7}[areaIndex];
        else if (this.isRightTeleport)
            return {3:2,2:1,13:12,8:13,9:8,4:9,5:4,15:14,10:15,11:10,6:11,7:6}[areaIndex];
    }
}

class LadderEntity extends Entity {
    len = 40;
    spriteId;

    read(view, offset) {
        this.spriteId = vword(view, offset + 14);
        this.type = vword(view, offset + 16);
        return super.read(view, offset);
    }
}

class EntityC extends Entity {
    len = 10;
}

class EntityD extends Entity {
    len = 10;
}

class EntityE extends Entity {
    len = 10;
}

class ResourcePack {
    resources = new Map;
    kindMap = new Map;

    read(view, offset) {
        // http://bytepointer.com/resources/win16_ne_exe_format_win3.0.htm
        const fileOffset = offset;
        offset += /*d*/(vdword(view, offset + 0x3C)); // MZ: NE header offset
        offset += /*d*/(vword(view, offset + 0x24)); // NE: resource table offset

        const ashift = vword(view, offset);
        offset += 2;

        let indexResource = null;
        let typeId;
        while ((typeId = vword(view, offset)) != 0) {
            const count = vword(view, offset + 2);
            offset += 8;

            for (let i = 0; i < count; i++) {
                const blockOffset = vword(view, offset);
                const blockLength = vword(view, offset+2);
                const resourceId = vword(view, offset+6);
                const resource = new Resource(
                    resourceId, typeId,
                    vslice(
                        view,
                        fileOffset + blockOffset << ashift,
                        fileOffset + blockLength << ashift),
                    this);
                this.resources.set(typeId << 16 | resourceId, resource);
                if (typeId == 0x800F)
                    indexResource = resource;
                // dd(`${offset}: ${i}/${count}: ${resource}`);
                offset += 12;
            }
        }

        let idLength;
        while ((idLength = vbyte(view, offset)) != 0) {
            dd(`${offset}: idLength=${idLength}`);
            offset += 1 + idLength;
        }

        let indexOffset = 0;
        let entryLength;
        while ((entryLength = vword(indexResource.data, indexOffset)) != 0) {
            const typeId = vword(indexResource.data, indexOffset+2);
            const groupId = vword(indexResource.data, indexOffset+4);
            const fourcc = vxd(vslice(indexResource.data, indexOffset+6, 4));
            const resource = this.resources.get(typeId << 16 | (0x8000 + groupId));
            // dd(`resource index: fourcc=${fourcc} groupId=${dh(groupId)}: ${resource}`);
            const groupMap = mg(this.kindMap, fourcc, new Map);
            groupMap.set(groupId, resource);
            indexOffset += entryLength;
        }

        // dd(this.resources);
        // dd(this.kindMap);
        // dd(vxd(this.kindMap.get("BLUE").get(0x157C).data));
        // dd(vdump(this.kindMap.get("BLUE").get(0x157C).data).map(x => dh(x, "")).join(" "));

        return offset;
    }

    get(fourcc, groupId) {
        return this.kindMap.get(fourcc).get(groupId).data;
    }
}

class Resource {
    module;
    id; typeId; data;

    constructor(id, typeId, data, module = null) {
        this.id = id;
        this.typeId = typeId;
        this.data = data;
        this.module = module;
    }

    toString() {
        return `[Resource id=${dh(this.id)} typeId=${dh(this.typeId)}: ${vxd(this.data).slice(0,16)}]`;
    }
}

class PartResource {
    level;
    definitionCount;
    slotCount;
    criticalSlotCount;
    definitions = [];
    slotLabels = new Map;
    criticalSlotLabels = new Set;

    constructor(level = null) {
        this.level = level;
    }

    read(view, offset) {
        this.definitionCount = vword(view, offset + 2);
        this.slotCount = vword(view, offset + 4);
        this.criticalSlotCount = vword(view, offset + 6);
        offset += 48 + 2 * this.criticalSlotCount;

        const criticalSet = new Set;
        const decorativeSet = new Set;
        for (let i = 0; i < this.definitionCount; i++) {
            const part = new PartDefinition(this);
            offset = part.read(view, offset);
            this.definitions.push(part);
            (part.critical ? criticalSet : decorativeSet).add(part.slotId);
        }

        const criticals = [...criticalSet].sort((p,q) => p - q);
        const decoratives = [...decorativeSet].sort((p,q) => p - q);
        for (const [i, slotId] of criticals.entries()) {
            this.slotLabels.set(slotId, String.fromCodePoint("A".codePointAt(0) + i));
            this.criticalSlotLabels.add(this.slotLabels.get(slotId));
        }
        for (const [i, slotId] of decoratives.entries())
            this.slotLabels.set(slotId, String.fromCodePoint("Z".codePointAt(0) - i));
        // dd(this.slotLabels);

        return offset;
    }

    slotLabel(slotId) {
        return this.slotLabels.get(slotId);
    }

    slotBestPerformances() {
        return this.definitions.reduce(
            (r, {slotId, performance}) =>
                mt(r, slotId, x => Math.max(x, performance), -Infinity),
            new Map);
    }

    slotBestCounts() {
        const best = this.slotBestPerformances();
        return this.definitions.reduce(
            (r, {slotId, performance}) =>
                mt(r, slotId, x => x + Number(performance == best.get(slotId)), 0),
            new Map);
    }
}

class PartDefinition {
    parent;
    slotId;
    performance;
    critical;

    constructor(parent = null) {
        this.parent = parent;
    }

    read(view, offset) {
        this.slotId = vword(view, offset + 8);
        this.performance = vword(view, offset + 10);
        this.critical = vword(view, offset + 14) != 0;
        return offset + 98;
    }

    get slotLabel() {
        return this.parent.slotLabels.get(this.slotId);
    }
}

/** Searches for the nearest RNG seed to a given base seed, with the user’s feedback about locations
    of parts in their spawn area. */
class Solver {
    /** True iff the solver is active and expecting input. */
    phase = false;

    /** Inverse of the canvas transformation matrix used when drawing the spawn area. */
    invertCtm = null;

    /** Range of RNG seed deltas to consider. To minimise ambiguity, this should be as narrow as
     *  possible while still allowing enough flexibility for user input speed or VM clock error. */
    universe = [-5n,10n];

    /** User input: known spawn area locations. */
    choices = new Set;

    /** Set of surviving deltas, with a cache of their spawn area locations. Surviving deltas are
     *  the subset of #universe whose spawn area locations are a superset of #choices. Caching their
     *  locations means we only need to simulate parts placement once for each seed. */
    deltas = new Map;

    /** Set of surviving locations, with a cache of their reference counts. Surviving locations are
     *  the union of all surviving deltas’ spawn area locations. Caching the number of deltas that
     *  refer to each location makes it easy to update this set whenever deltas are eliminated. */
    locations = new Map;

    reset() {
        this.phase = false;
        this.choices.clear();
        this.deltas.clear();
        this.locations.clear();
    }

    start(baseSeed, level) {
        this.reset();
        this.phase = true;
        const spawnArea = level.world.spawnArea;
        for (let delta = this.universe[0]; delta <= this.universe[1]; delta++) {
            const locations = level.generateAndPlace(baseSeed + delta)
                .filter(([, a]) => a == spawnArea.areaIndex)
                .map(([, , l]) => l);
            this.deltas.set(delta, new Set(locations));
            for (const location of locations)
                mt(this.locations, location, x => x + 1, 0);
        }
        dd(this.deltas);
        dd(this.locations);
    }

    click(location) {
        if (!this.phase || !this.locations.has(location))
            return;

        this.choices.add(location);

        const choices = /*dd*/([...this.choices]);
        for (let delta = this.universe[0]; delta <= this.universe[1]; delta++) {
            if (!this.deltas.has(delta))
                continue;
            const locations = this.deltas.get(delta);
            if (choices.some(x => !locations.has(x))) {
                this.deltas.delete(delta);
                for (const location of locations) {
                    const count = this.locations.get(location);
                    if (count == 1)
                        this.locations.delete(location);
                    else
                        this.locations.set(location, count - 1);
                }
            }
        }
        dd(this.deltas);
        dd(this.locations);

        // Check that the sets of chosen and surviving spawn area locations are equal. For early
        // solutions with fewer choices, we could instead check for (this.candidates.size == 1) and
        // (choices.length <= this.locations.size) and (choices.every(...)), but asking the user to
        // be explicit about the redundant parts might yield a UX that feels more reliable.
        if (choices.length == this.locations.size && choices.every(x => this.locations.has(x))) {
            this.phase = false;
            return [...this.deltas.keys()].sort((p,q) => cmp(abs(p),abs(q)) || sgn(q)-sgn(p))[0];
        } else if (this.deltas.size == 0) {
            alert("BUG: failed to find seed (this should never happen!)");
            this.reset();
        }

        return null;
    }

    possibleChoices() {
        return [...this.locations.keys()].filter(x => !this.choices.has(x));
    }
}

const GIZMO = new ResourcePack;
// const AUTO = new ResourcePack;
// const PLANE = new ResourcePack;
// const AE = new ResourcePack;
const worlds = [...Array(9).keys()].map(() => new World);
const levels = [...Array(15).keys()].map(i => new Level(i));

const edge = 16;
const between = 64;
const mainGameArea = {x:0,y:16,w:512,h:300};
const screen = {x:0,y:0,w:512,h:384};
const solver = new Solver;
const completedSlots = new Set;

async function main() {
    GIZMO.read(await loadraw("GIZMO.DAT"), 0);
    // AUTO.read(await loadraw("AUTO.DAT"), 0);
    // PLANE.read(await loadraw("PLANE.DAT"), 0);
    // AE.read(await loadraw("AE.DAT"), 0);

    for (const [i, world] of worlds.entries())
        world.read(GIZMO.get("AREA", 0x579 + i), 0);

    for (const level of levels)
        level.load(GIZMO, worlds);

    const [levelField, seedField, deltaField, seedNow, startSolver, nextLevel] = document.querySelectorAll("select, input, button");
    const seedNowHandler = () => {
        seedField.value = `${Math.floor(Date.now() / 1000)}`;
        if (solver.phase)
            solver.start(BigInt(seedField.value), levels[levelField.selectedIndex]);
        completedSlots.clear();
        draw();
    };
    const startSolverHandler = () => {
        solver.start(BigInt(seedField.value), levels[levelField.selectedIndex]);
        completedSlots.clear();
        draw();
    };
    const nextLevelHandler = () => {
        levelField.selectedIndex = (levelField.selectedIndex + 1) % levelField.options.length;
        if (solver.phase)
            solver.start(BigInt(seedField.value), levels[levelField.selectedIndex]);
        completedSlots.clear();
        draw();
    };
    seedNow.addEventListener("click", seedNowHandler);
    startSolver.addEventListener("click", startSolverHandler);
    nextLevel.addEventListener("click", nextLevelHandler);
    addEventListener("keypress", event => {
        if (event.target.nodeName == "INPUT")
            return;
        const key = event.key.toUpperCase();
        switch (key) {
            case "3":
                seedNowHandler();
                return;
            case "4":
                startSolverHandler();
                return;
            case "5":
                nextLevelHandler();
                return;
        }
        if (!/^[A-Za-z]$/.test(key))
            return;
        if (!levels[levelField.selectedIndex].parts.criticalSlotLabels.has(key))
            return;
        if (completedSlots.has(key))
            completedSlots.delete(key);
        else
            completedSlots.add(key);
        draw();
    });

    const canvas = document.querySelector("canvas");
    canvas.addEventListener("click", ({target, clientX: x, clientY: y}) => {
        const element = target.getBoundingClientRect();
        const level = levels[levelField.selectedIndex];
        const area = level.world.spawnArea;

        //  0. (r,c) = divmod(location,columnCount)
        //  1. (x,y) = (c,r) * (rowHeight,columnWidth)
        //  2. (x',y') = (x,y) + (xLeftForPart,yTopForPart)
        //  3. canvas transformation
        //      a. (x'',y'') = (x',y') - mainGameArea.(x,y)
        //      b. (x''',y''') = (x'',y'') * (2,2)
        //      c. (x'''',y'''') = (x''',y''') + (edge,edge)
        //  4. (x''''',y''''') = (x'''',y'''') + element.(x,y)

        let point = new DOMPoint(x,y);
        point.x -= element.x; point.y -= element.y;
        point = point.matrixTransform(solver.invertCtm);
        point.x -= area.xLeftForPart; point.y -= area.yTopForPart;
        point.x /= area.columnWidth; point.y /= area.rowHeight;
        x = point.x; y = point.y;

        const [r, c] = [(y|0), (x|0)];
        const location = area.columns * r + c;
        const hit = y - r < area.partHeight / area.rowHeight;
        const can = area.canPlacePart(location);
        // dd(r, c, hit, can, location);

        if (hit && can) {
            const delta = solver.click(location);
            if (delta != null)
                deltaField.value = `${delta}`;
            draw();
        }
    });

    addEventListener("change", () => void reset());
    addEventListener("input", () => void reset());
    reset();
}

function drawArea(level, areaId, placements, baseSeed, canvas) {
    const area = level.world.areas[areaId];

    for (let i = 0; i < area.rows; i++) {
        canvas.fillStyle = "#000000FF";
        const y = area.yTopForFloor + area.rowHeight * i;
        canvas.fillRect(0, y, 512, area.floorHeight);
    }

    // for (let i = 0; i < area.rows; i++) {
    //     for (let j = 0; j < area.columns; j++) {
    //         const x0 = area.columnStart + area.columnWidth * j;
    //         const y0 = area.rowStart + area.rowHeight * i;
    //         canvas.strokeStyle = "#00000040";
    //         canvas.strokeRect(x0, y0, area.columnWidth, area.rowHeight);
    //     }
    // }

    for (const e of area.structuralGroups[0]) {
        canvas.fillStyle = "#000000FF";
        if ([0x04,0x05,0x06].includes(e.type))
            canvas.clearRect(e.x, e.y, e.w, e.h);
        else if ([0x10].includes(e.type))
            canvas.fillRect(e.x, e.y, e.w, e.h);
        else if (e.isTeleport && !e.isDoorTeleport
                && (e.isBlockedTeleport || e.teleportDestination == null))
            canvas.fillRect(e.x, e.y, e.w, e.h);
        if ([0x04,0x05,0x06,0x0A,0x0B,0x0C,0x0D].includes(e.type))
            ctype(canvas, e, area.areaIndex);
    }

    for (let i = 0; i < area.rows; i++) {
        for (let j = 0; j < area.columns; j++) {
            const location = area.columns * i + j;
            const {x,y,w,h} = area.partCollisionRect(location);

            if (area.canPlacePart(location))
                canvas.strokeStyle = "#0000FFFF";
            else
                // canvas.strokeStyle = "#FF0000FF";
                canvas.strokeStyle = "#FF000000";

            canvas.strokeRect(x, y, w, h);
        }
    }

    canvas.fillStyle = "#000000FF";
    canvas.strokeStyle = "#000000FF";

    for (const e of area.structuralGroups[0]) {
        if ([0x03,0x12,0x08].includes(e.type)) continue;
        if ([0x04,0x05,0x06].includes(e.type)) continue;
        if ([0x10].includes(e.type)) continue;
        if (e.isTeleport && !e.isDoorTeleport) continue;
        canvas.strokeRect(e.x, e.y, e.w, e.h);
        ctype(canvas, e, area.areaIndex);
    }

    for (const e of area.ladderGroups[0]) {
        canvas.strokeRect(e.x, e.y, e.w, e.h);
        ctype(canvas, e, area.areaIndex);
    }

    const bestPerformances = level.parts.slotBestPerformances();
    const bestCounts = level.parts.slotBestCounts();

    if (solver.phase) {
        if (areaId == level.world.pageCount) {
            for (const location of solver.choices)
                drawPart(location, `+`, "#38703880", "#387038FF", area, canvas);
            for (const location of solver.possibleChoices())
                drawPart(location, `?`, "#80808080", "#808080FF", area, canvas);
        }
    } else {
        for (const [partId, , location] of placements.filter(([_,a]) => a == area.areaIndex)) {
            const part = level.parts.definitions[partId] ?? null;
            const text = part != null
                ? `${part.slotLabel}${part.performance}`
                : `${partId}`;
            if (part != null && completedSlots.has(part.slotLabel))
                continue;
            if (!area.canPlacePart(location))
                drawPart(location, text, "#FF000080", "#FF0000FF", area, canvas);
            else if (part == null)
                drawPart(location, text, "#C0C00080", "#C0C000FF", area, canvas);
            else if (!part.critical || part.performance != bestPerformances.get(part.slotId))
                drawPart(location, text, "#80808080", "#808080FF", area, canvas);
            else if (bestCounts.get(part.slotId) > 1)
                drawPart(location, text, "#66339980", "#663399FF", area, canvas);
            else
                drawPart(location, text, "#0000FF80", "#0000FFFF", area, canvas);
        }
    }

    if (solver.phase) {
        if (areaId != level.world.pageCount) {
            const {x,y,w,h} = mainGameArea;
            canvas.fillStyle = "#FFFFFFC0";
            canvas.fillRect(x, y, w, h);
        }
    } else {
        canvas.font = "bold 256px sans-serif";
        canvas.fillStyle = "#00000028";
        canvas.textBaseline = "middle";
        canvas.textAlign = "center";
        const {w,h} = mainGameArea;
        canvas.fillText(`${area.areaIndex}`, w / 2, h / 2);
    }
}

function drawPart(location, text, rectColor, textColor, area, canvas) {
    const {x,y,w,h} = area.partSpriteRect(location);
    const size = area.areaIndex > 3 ? 36 : 18;
    canvas.font = `bold ${size}px sans-serif`;
    canvas.textBaseline = "bottom";
    canvas.textAlign = "right";
    canvas.fillStyle = rectColor;
    canvas.fillRect(x, y, w, h);
    canvas.fillStyle = textColor;
    canvas.fillText(text, x + w, y);
}

function draw() {
    const levelId = document.querySelector("#level").selectedIndex;
    const level = levels[levelId];

    const seed = BigInt(document.querySelector("#seed").value);
    const delta = BigInt(document.querySelector("#delta").value);
    const placements = level.generateAndPlace(seed + delta);
    const world = level.world;
    const {x,y,w,h} = mainGameArea;
    const pages = world.areaCount / 4 - 1;

    const element = document.querySelector("canvas");
    element.width = edge + 2 * pages * screen.w + edge;
    element.height = edge + 2 * h + between + 2 * h + edge;

    const canvas = element.getContext("2d");
    canvas.resetTransform();
    canvas.clearRect(0, 0, element.width, element.height);

    const completedSlotLabels = [...level.parts.criticalSlotLabels]
        .sort()
        .map(x => completedSlots.has(x) ? x : "_")
        .join("");
    canvas.resetTransform();
    canvas.translate(edge, edge + 2 * h);
    canvas.font = "36px monospace";
    canvas.fillStyle = "#000000";
    canvas.textBaseline = "middle";
    canvas.textAlign = "left";
    canvas.fillText(`[${completedSlotLabels}]`, 0, between / 2);

    for (const area of world.areas.slice(1, 1 + pages)) {
        const i = area.areaIndex;
        canvas.resetTransform();
        canvas.translate(edge, edge);
        canvas.scale(2, 2);
        canvas.translate(-x, -y);
        canvas.translate((pages - i) * 512, 0);
        if (i == pages)
            solver.invertCtm = canvas.getTransform().inverse();
        drawArea(level, i, placements, seed, canvas);
    }

    for (const area of world.areas.slice(4)) {
        const i = area.areaIndex;
        canvas.resetTransform();
        canvas.translate(edge, edge + 2 * h + between);
        canvas.scale(1, 1);
        const dx = 1024 * (Math.floor(i / 4) - 1) + 512 * Number(i % 2 == 0);
        const dy = h * Number(i % 4 > 1);
        canvas.translate(dx, dy - y);
        drawArea(level, i, placements, seed, canvas);
    }

    if (solver.phase) {
        canvas.resetTransform();
        canvas.translate(edge, edge);
        canvas.scale(2, 2);
        canvas.translate(-x, -y);
        canvas.font = "bold 48px sans-serif";
        canvas.fillStyle = "#387038FF";
        canvas.textBaseline = "top";
        canvas.textAlign = "center";
        canvas.fillText(
            `click ${level.spawnAreaPartsCount - solver.choices.size} more parts`,
            w / 2, h + 48);
    }
}

function reset() {
    solver.reset();
    completedSlots.clear();
    draw();
}

main();
